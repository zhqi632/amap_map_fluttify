import 'package:flutter/cupertino.dart';

/// @author: pengboboer
/// @createDate: 6/17/21



/// 升级所有的库后，这些常量在项目中找不到了
/// 我估计是这个库把这些常量给改了 'package:decorated_flutter/decorated_flutter.dart';
/// 反正就是个demo也不影响啥，随便写几个数顶替一下吧
const double kSpaceBig = 10;
const double kSpaceNormal = 2;

final Widget SPACE_BIG = Container(height: 10);
final Widget SPACE_NORMAL = Container(height: 10);
final Widget kDividerZero = Container(height: 10, color: Color(0xffe5e5e5));
final Widget kDividerTiny = Container(height: 10, color: Color(0xffe5e5e5));